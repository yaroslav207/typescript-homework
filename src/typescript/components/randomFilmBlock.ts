import { Film } from "../interface";

export function createRandomFilmBlock (film: Film): void {
    const randomFilmBlock = document.querySelector('#random-movie');
    console.log(randomFilmBlock);
    if(!randomFilmBlock){
        return
    }

    randomFilmBlock.innerHTML = (`<div class="row py-lg-5">
                    <div
                        class="col-lg-6 col-md-8 mx-auto"
                        style="background-color: #2525254f"
                    >
                        <h1 id="random-movie-name" class="fw-light text-light">${film.title}</h1>
                        <p id="random-movie-description" class="lead text-white">
                            ${film.overview}
                        </p>
                    </div>
                </div>`)
}