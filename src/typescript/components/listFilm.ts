import {filmListCart} from "./cartFilm";
import {filmService} from "../services/filmService";
import {Film} from "../interface";
import {getRandomNumber} from "../helpers/random";
import {createRandomFilmBlock} from "./randomFilmBlock";
import {createFavoriteFilmsBlock} from "./favoriteFilmsBlock";

export async function  getFilms(sortBy: 'popular' | 'top_rated' | 'upcoming' = 'popular', page: number): Promise<void> {
        const films = await filmService.getFilms(sortBy, page);
        const clearFilmContainer = page === 1;
        fillFilmContainer(clearFilmContainer)(films)
}

export async function searchFilms(searchFilm: string | undefined): Promise<void> {
    if(!searchFilm){
        return
    }
    const films = await filmService.searchFilm(searchFilm);
    fillFilmContainer(true)(films)
}

export function fillFilmContainer (clearFilmContainer: boolean) {
    const filmContainer = document.getElementById('film-container');
    return (films: Film[]) => {
    if (filmContainer){
        if(clearFilmContainer){
            filmContainer.innerHTML = "";
        }
        listFilm(films).map((item) => {
            filmContainer.appendChild(item)
        })
    }
}}

function listFilm (films: any[]): HTMLElement[] {
    return films.map((film) => {
        return filmListCart(film)
    })
};

function createRandomFilm(films: Film[]) {
    const randomNumber = getRandomNumber();
    createRandomFilmBlock(films[randomNumber])
};

export async function startApp(sortBy: 'popular' | 'top_rated' | 'upcoming' = 'popular', page: number){
    const films = await filmService.getFilms(sortBy, page);
    const clearFilmContainer = page === 1;
    fillFilmContainer(clearFilmContainer)(films);
    createRandomFilm(films);
    createFavoriteFilmsBlock();
}
