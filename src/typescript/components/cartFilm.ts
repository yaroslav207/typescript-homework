import { Film } from "../interface";
import { filmService } from "../services/filmService";
import { createElement } from "../helpers/domHelper";
import {createFavoriteFilmsBlock} from "./favoriteFilmsBlock";

function cartFilm(filmInfo : Film): HTMLElement {
    const setReaction = (id: number) => {
        if (filmService.checkFavoriteList(id)){
            like.firstElementChild?.classList.remove('active');
            filmService.deleteWithFavoriteList(id)
        } else {
            like.firstElementChild?.classList.add('active');
            filmService.addToFavoriteList(id)
        }
        createFavoriteFilmsBlock();
    };


    const cart = createElement({
        tagName: 'div',
        className: 'card shadow-sm',
        attributes: {}
    });
    const like = createElement({
        tagName: 'div',
        className: 'position-absolute p-2',
    });
    const image = createElement({
        tagName: 'img',
        attributes: {
            src: `https://image.tmdb.org/t/p/original/${filmInfo.image}`
        }
    });
    const cartBody = createElement({
        tagName: 'div',
        className: 'card-body'
    });

    cartBody.innerHTML = (`<p class="card-text truncate">
                                        ${filmInfo.overview}
                                    </p><div
                                        class="
                                            d-flex
                                            justify-content-between
                                            align-items-center
                                        "
                                    >
                                        <small class="text-muted">${filmInfo.date}</small>
                                    </div>`);
    like.innerHTML = (`<svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    stroke="red"
                                    width="50"
                                    height="50"
                                    class="bi bi-heart-fill position-absolute p-2 ${ filmService.checkFavoriteList(filmInfo.id) ? "active" : ""}"
                                    viewBox="0 -2 18 22"
                                >
                                    <path
                                        fill-rule="evenodd"
                                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                                    />
                                </svg>`);

    like.addEventListener('click', () => setReaction(filmInfo.id));

    cart.appendChild(like);
    cart.appendChild(image);
    cart.appendChild(cartBody);

    return cart
}

export function filmListCart(film: Film): HTMLElement {
    const cartBlock = createElement({
        tagName: 'div',
        className: 'col-lg-3 col-md-4 col-12 p-2',
        attributes: {}
    });
    const cart = cartFilm(film);
    cartBlock.appendChild(cart);
    return cartBlock;
}

export function favoriteFilmListCart(film: Film): HTMLElement {
    const cartBlock = createElement({
        tagName: 'div',
        className: 'col-12 p-2',
        attributes: {}
    });
    const cart = cartFilm(film);
    cartBlock.appendChild(cart);
    return cartBlock;
}

