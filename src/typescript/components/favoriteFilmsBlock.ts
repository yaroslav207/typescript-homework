import {getFavoriteList} from "../helpers/favoriteList";
import {filmService} from "../services/filmService";
import {favoriteFilmListCart} from "./cartFilm";

export async function createFavoriteFilmsBlock() {
    const listFavoriteFilms = await createListFavoriteFilms();
    const favoriteFilms = listFavoriteFilms?.map((film) => favoriteFilmListCart(film));
    const favoriteFilmsBlock = document.querySelector('#favorite-movies');

    if(!favoriteFilmsBlock){
        return
    }
    favoriteFilmsBlock.innerHTML = '';

    console.log(favoriteFilms);

    favoriteFilms?.map((item) => {
        favoriteFilmsBlock.appendChild(item);
    })


}

async function createListFavoriteFilms() {
    const idsFavoriteList = getFavoriteList();
    const favoriteList = [];
    if(!idsFavoriteList){
        return null
    }
    for(const value of idsFavoriteList){
        const film = await filmService.getFilmDetails(value);
        favoriteList.push(film)
    }
    return  favoriteList;
}