interface Attributes {
    onclick?: () => void,
    [key: string]: any,
}

interface Arguments{
    tagName: string
    className?: string
    attributes?: Attributes
}

export function createElement( arg: Arguments ) {
    const {tagName, className, attributes} = arg;
    const element = document.createElement(tagName);

    if (attributes) {
        Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));
    }

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    return element;
}