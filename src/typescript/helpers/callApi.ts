const BASE_URL = 'https://api.themoviedb.org/3';
const API_KEY = 'ba9b515fe01d43bf80c23c4e1a66e533';

interface Params {
    [keys: string]: string
}

export function callApi(endPoints: string, params?: Params) {
    let paramsUrl = '';
    for(const key in params){
        paramsUrl += `&${key}=${params[key]}`
    }
    return  fetch(`${BASE_URL}${endPoints}?api_key=${API_KEY}${paramsUrl}`, )
                .then(result => result.json())
}