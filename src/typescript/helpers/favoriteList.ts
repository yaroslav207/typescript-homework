export function setFavoriteList(favoriteList: number[]){
    localStorage.setItem("favoriteList", JSON.stringify(favoriteList));
}

export function getFavoriteList(): number[] | undefined{
    return JSON.parse(<string>localStorage.getItem("favoriteList"));
}