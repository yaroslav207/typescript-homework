import {callApi} from "../helpers/callApi";
import {Film} from "../interface";
import {getFavoriteList, setFavoriteList} from "../helpers/favoriteList";


class FilmService {

    private apiMapper(film: any): Film{
        return {
            id: film.id,
            title: film.title,
            image: film.poster_path,
            overview: film.overview,
            date: film.release_date
        }
    }

    async getFilms(sortBy: 'popular' | 'top_rated' | 'upcoming', page: number) {
        const endpoint = `/movie/${sortBy}`;
        const apiResult = await callApi(endpoint, {page: page+''});

        return apiResult.results.map(this.apiMapper)
    }

    async getFilmDetails(id:number): Promise<Film> {
        const endpoint = `/movie/${id}`;
        const apiResult = await callApi(endpoint);

        return this.apiMapper(apiResult);
    }

    async searchFilm(search: string) {
        const endpoint = '/search/movie';
        const apiResult = await callApi(endpoint, {query: search});
        return apiResult.results.map(this.apiMapper)
    }

    addToFavoriteList(id: number): void{
        let newFavoriteList: number[];
        const favoriteList = getFavoriteList();

        if(favoriteList){
            newFavoriteList = [...favoriteList, id]
        } else {
            newFavoriteList = [ id ]
        }

        setFavoriteList(newFavoriteList)
    }

    deleteWithFavoriteList(id: number): void{
        let newFavoriteList: number[];
        const favoriteList = getFavoriteList();

        if(favoriteList){
            newFavoriteList = favoriteList.filter(item => item !== id)
        } else {
            newFavoriteList = []
        }

        setFavoriteList(newFavoriteList)
    }

    checkFavoriteList(id: number): boolean{
        const favoriteList = getFavoriteList();
        return !!favoriteList?.includes(id);
    }

}

export const filmService = new FilmService();