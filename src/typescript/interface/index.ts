export interface Film {
    id: number
    title: string
    image: string
    overview: string
    date: Date
}