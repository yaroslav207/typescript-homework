import {getFilms, searchFilms, startApp} from "./typescript/components/listFilm";

export async function render(): Promise<void> {
    // TODO render your app here
    type SortBy = 'popular'|'upcoming'|'top_rated';

    let sortBy: SortBy = 'popular';
    let page = 1;

    const setSortBy = (typeSort: SortBy) => {
        sortBy = typeSort;
    };
    const setPage = (newPage:number) => {
        page = newPage;
    };

    function handleSortButton(typeSort: SortBy){
        setSortBy(typeSort);
        setPage(1);
        getFilms(typeSort, page);
    }
    function handleLoadMore() {
        setPage(page + 1);
        console.log(page);
        console.log(sortBy);
        getFilms(sortBy, page);
    }

    function handleSubmitForm(event: any){
        event.preventDefault();
        const formData = new FormData(event.target);
        const search = formData.get('search-input')?.toString();
        searchFilms(search);
    }

    const buttonPopular = document.querySelector('[for="popular"]');
    const buttonUpcoming = document.querySelector('[for="upcoming"]');
    const buttonTopRated = document.querySelector('[for="top_rated"]');
    const fromSearch = document.querySelector('.form-search');
    const loadMoreButton = document.querySelector('#load-more');

    buttonPopular?.addEventListener('click', () => handleSortButton('popular'));
    buttonUpcoming?.addEventListener('click', () => handleSortButton('upcoming'));
    buttonTopRated?.addEventListener('click', () => handleSortButton('top_rated'));
    fromSearch?.addEventListener('submit', (e) => handleSubmitForm(e));
    loadMoreButton?.addEventListener('click', handleLoadMore);

    startApp('popular', page);

}
